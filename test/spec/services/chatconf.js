'use strict';

describe('Service: chatConf', function () {

  // load the service's module
  beforeEach(module('chatbotApp'));

  // instantiate service
  var chatConf;
  beforeEach(inject(function (_chatConf_) {
    chatConf = _chatConf_;
  }));

  it('should do something', function () {
    expect(!!chatConf).toBe(true);
  });

});

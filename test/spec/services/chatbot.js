'use strict';

describe('Service: chatBot', function () {

  // load the service's module
  beforeEach(module('chatbotApp'));

  // instantiate service
  var chatBot;
  beforeEach(inject(function (_chatBot_) {
    chatBot = _chatBot_;
  }));

  it('should do something', function () {
    expect(!!chatBot).toBe(true);
  });

});

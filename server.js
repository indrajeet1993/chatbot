/**
 * code created by indrajeet ambadekar
 */

var express = require('express');
var fs      = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var cors = require('cors');
var app     = express();
app.use(cors());

app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});
app.get('/messages', function(req, res){
  var payload = req.query;
  url = 'http://www.personalityforge.com/api/chat/?apiKey='+payload.apiKey+"&chatBotID="+payload.chatBotID+"&externalID="+payload.externalID+"&message="+payload.message;
  request(url, function(error, response, html){
    var list = response.body;
    console.log(response)
    if(response){
        res.send({status:'ok','payload':list});
      }
      else{
        res.send({status:'failed','payload':error});

      }
    return list;
  })
}).get('/moviesCast/:hash', function(req, res){
  url = 'http://www.imdb.com/chart/top';
  console.log(req.params.hash)
  request(req.params.hash, function(error, response, html){
    var cast={}
    if(!error){
      var $ = cheerio.load(html);
      cast.pic = $('#img_primary').find('img').attr('src')
      cast.name = $('#overview-top').find('h1').find('.itemprop').text().trim();

    }


    res.send(cast);
  })
})


app.listen('8081')

console.log('Server running at 8180');
exports = module.exports = app;

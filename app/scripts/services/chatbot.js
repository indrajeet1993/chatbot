'use strict';

/**
* @ngdoc service
* @name chatbotApp.chatBot
* @description
* # chatBot
* Service in the chatbotApp.
*/
angular.module('chatbotApp')
.service('chatBotService', function ($q, $http,chatConf) {
	return {
		fetchMessages:function(user,gender){
			var apiKey='6nt5d1nJHkqbkphe'
      var deferred = $q.defer();
      $http({
          method: 'GET',
          url: 'http://localhost:8081/messages?apiKey='+apiKey+'&message=Hi '+Date.now()+'&chatBotID=63906&externalID='+user+'&gender='+gender,
          data: '',
          'async': true,
          'cache': false,
          'global': false,
          headers: {}
        }).then(function(response) {
          deferred.resolve(response.data);
        })
        .catch(function(response, status) {
          var rejection = {
            response: response,
            status: status
          }
          deferred.reject(rejection);
        });

      return deferred.promise;
    },
    sendMessages:function(user,msg){
      var apiKey='6nt5d1nJHkqbkphe'
      var deferred = $q.defer();
      $http({
          method: 'GET',
          url: 'http://localhost:8081/messages?apiKey='+apiKey+'&message='+msg+'&chatBotID=63906&externalID='+user,
          data: '',
          'async': true,
          'cache': false,
          'global': false,
          headers: {}
        }).then(function(response) {
          deferred.resolve(response.data);
        })
        .catch(function(response, status) {
          var rejection = {
            response: response,
            status: status
          }
          deferred.reject(rejection);
        });

      return deferred.promise;
    }
	}
});

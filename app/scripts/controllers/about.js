'use strict';

/**
 * @ngdoc function
 * @name chatbotApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the chatbotApp
 */
angular.module('chatbotApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

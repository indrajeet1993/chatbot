'use strict';

/**
 * @ngdoc function
 * @name chatbotApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the chatbotApp
 */
 angular.module('chatbotApp')
 .controller('LoginCtrl', function ($scope, ngToast, $location, $rootScope) {
	$rootScope.currentState = "login";
 	$scope.gender = 'M';
 	$scope.username = "chirag1";
 	$scope.loginUser = function(){
 		if($scope.username===undefined){
 			ngToast.create({
 				className: 'danger',
 				content: 'Please provide a username'
 			});
 		}
 		else{
 			$scope.username = $scope.username.replace(/ /g,"_");
 			$location.path('/messages/'+JSON.stringify({'u':$scope.username,'s':$scope.gender}));
 		}
 	}
 	$scope.genderSelect = function($event,gdr){
 		$('.genderPicker').removeClass('active');
 		$($event.target).closest('.genderPicker').addClass('active');
 		$scope.gender = gdr;
 	}
 });

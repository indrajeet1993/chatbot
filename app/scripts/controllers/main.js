'use strict';

/**
* @ngdoc function
* @name chatbotApp.controller:MainCtrl
* @description
* # MainCtrl
* Controller of the chatbotApp
*/
angular.module('chatbotApp')
.controller('MainCtrl', function ($scope, $location, $routeParams, chatBotService, $timeout, $rootScope) {
	$scope.messages=[];
	$rootScope.currentState = "messages";
	function SetMsgs(){
		localStorage.setItem('messages',JSON.stringify($scope.messages));
		$timeout(function(){
			$(".chats").animate({
				scrollTop: $('.chats')[0].scrollHeight - $('.chats')[0].clientHeight
			}, 300);
		},100)
	}

	if(localStorage.messages != undefined){
		// $scope.messages = JSON.parse(localStorage.messages);
		localStorage.clear();
	}
	var $currUser = JSON.parse($routeParams.user);

	function getMsg(){
		chatBotService.fetchMessages($currUser.u,$currUser.s).then(function(resp){
			if(resp.status==='ok'){
				var currMsg = JSON.parse(resp.payload).message;
				currMsg.owner = 'bot';
				currMsg.timeStamp = Date.now();
				currMsg.id = $scope.messages.length;
				$scope.messages.push(currMsg);
				SetMsgs();
			}
		}).catch(function(reason){
			console.log(reason)
		});
		
	}

	if($currUser.u === undefined){
		$location.path('/');
	}else{
		getMsg();
	}

	$scope.setChatBoxHeight = function(){
		return {
			'height':$(window).height()-150,
			'overflow-y':'auto'
		}
	}

	$scope.sendMsg = function(){
		if($scope.textMsg!=undefined){
			$scope.messages.push({chatBotName: $currUser.u, chatBotID: 63906, message: $scope.textMsg, emotion: "",owner:'user',timeStamp:Date.now(),id:$scope.messages.length})
			SetMsgs();
			$timeout(function(){
				$scope.textMsg = "";
				getMsg();
			},500);
		}
	}

	$scope.watchKeyup = function($event){
		if($event.keyCode===13){
			$scope.sendMsg();
		}
	}

	$scope.getMsgOwner = function(msg){
		if(msg.owner==='bot'){
			return "chatMsg chatMsgLeft";
		}
		else{
			return "chatMsg chatMsgRight";
		}
	}

});
